# TextReuseforTranscription

Text reuse detection framework in the assistance of automatic transcription corrections

## Software Dependencies

- [ ] [Install](https://pypi.org/project/virtualenv/) virtualenv
- [ ] [Install](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) git
- [ ] [Install](https://linuxize.com/post/how-to-install-python-3-7-on-ubuntu-18-04/) python3.7


## Create a Virtual Environment

```
virtualenv venv -p python3.7
source venv/bin/activate
```

## Cloning the project

```
git clone https://gitlab.com/millerhadar/textreusefortranscription
cd textreusefortranscription/
pip install -r requirements.txt
```

## Usage

The basic usage allows you to scan the transcripted lines (an excel sheet). It locates a suspected text reuse source for each line and aligns the texts in between.

```
python transcription_correction.py -p data/ -f test_data -e cloud
```

Please note: this code relies on an existing positional inverted index. we provide such index on the cloud, however with limited access. if you wish to use our cloud based index, please contact us.
