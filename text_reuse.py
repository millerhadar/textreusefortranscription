from functools import wraps
import os
import time
import datetime
import logging
import logging.handlers
import pandas as pd
import pickle
import json
import io
import unicodedata
import re
from elasticsearch import Elasticsearch
from elastic_extention import elasticE
import io
import numpy as np
from hebrew_numbers import gematria_to_int,int_to_gematria
import heb_tokenizer
from sqlalchemy import create_engine
import psycopg2
import requests


try:
    from django.conf import settings
except ImportError:
    try:
        from local_django_settings import django_setting
        settings = django_setting()
    except ImportError:
        pass

try:
    from local_django_settings import get_text_reuse_rules
except ImportError:
    pass

SPLIT_RE = re.compile(r'[^\'\"\״a-zA-Z0-9א-ת]')


def tokenize(text):
    # yield from SPLIT_RE.split(text)
    yield from [x[1] for x in heb_tokenizer.tokenize(text)]


def ftokens(text, spliter=None):
    if spliter:
        [x for x in text.split(spliter) if x != '']
    else:
        #ret = [x for x in SPLIT_RE.split(text) if x != '']
        ret = [x[1] for x in heb_tokenizer.tokenize(text) if x[0] in ['HEB']]
    return ret


def clean_special_chars(src):
    if src:
        src = src.replace("׳", "'")
        src = src.replace('״','"')
        for sym in ['־' , ":" , "׃"]:
            src = src.replace(sym, " ")
        for sym in ['(' , ")" , "{", "}", "\u200e"]:
            src = src.replace(sym, "")
    return src

def diacritic_to_matres_lectionis(src, DEBUG = False):
    '''

    :param src: Text to remove the NIKUD
    :param DEBUG: Print for debugging
    :return: The text where diacritics replaced with full orthography ( י,ו)
    '''
    import unicodedata

    diacritic = {14: {"replace": "י", "exceptions": ["י", "א"]},
                 19: {"replace": "ו", "exceptions": ["ו", "א"]},
                 20: {"replace": "ו", "exceptions": ["ו"]},
                 }

    uc = unicodedata.normalize('NFKD', src)
    new_word = ""
    prev_diacritic = None
    prev_exception = []
    for i in uc:
        if unicodedata.combining(i) == 0:
            if prev_diacritic and i not in prev_exception:
                new_word += prev_diacritic
            new_word += i
            prev_diacritic = None
            prev_letter = i
        else:
            if unicodedata.combining(i) in diacritic:
                if prev_letter != diacritic[unicodedata.combining(i)]["replace"]:
                    prev_diacritic = diacritic[unicodedata.combining(i)]["replace"]
                    prev_exception = diacritic[unicodedata.combining(i)]["exceptions"]
        if DEBUG:
            print(i,unicodedata.combining(i), type(unicodedata.combining(i)))
    return new_word

def remove_nikud(src):
    '''
    If the inner variable nikud is set to True, the function
    returns the text with no Nikud
    :param src: The text to remove Nukud
    :return: the text with no Nikud.
    '''
    try:
        normalized = unicodedata.normalize('NFKD', src)
        flattened = ''.join([c for c in normalized if not unicodedata.combining(c)])
        return flattened
    except:
        return ""

def remove_html(src):
    p = re.compile(r'<.*?>')
    return p.sub('', src)

def remove_references(src):
        books = '''בראשית|שם|שמות|ויקרא|במדבר|דברים|יהושע|שופטים|שמואל|מלכים|ישעיהו|ירמיהו|יחזקאל|הושע|יואל|עמוס|עובדיה|יונה|מיכה|נחום|חבקוק|צפניה|חגי|זכריה|מלאכי|תהלים|משלי|איוב|שיר השירים|רות|איכה|קהלת|אסתר|דניאל|עזרא|נחמיה|דברי הימים|ישעיה|ירמיה|תהילים|שה\\"ש|שה\"ש'''
        p = re.compile('\(([ ]*('+books+').*?)\)')
        cleaned_src = p.sub('', src)
        references = [clean_special_chars(x.group()).replace("'","").replace('"','') for x in re.finditer('\(([ ]*('+books+').*?)\)', src) ]
        return cleaned_src, references

def clean_text(sentence):
    ret, _ = remove_references(sentence)
    ret = remove_nikud(ret)
    ret = remove_html(ret)
    ret = clean_special_chars(ret)
    return ret



def lemmatize(word):
    return word


def synonyms(word):
    return []


def translations(word):
    return []


def ones_sequence(a, threshold = 0):
    # Create an array that is 1 where a is greater than threshold,
    # and pad each end with an extra 0.
    iszero = np.concatenate(([0], np.greater(a, threshold).view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    # Runs start and end where absdiff is 1.
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
    return ranges


class my_log():
    def __init__(self):
        log_path = "log/"
        timestr = time.strftime("%Y%m%d-%H%M")
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        log_filename = log_path + timestr + '.log'
        self.SDST = logging.getLogger('Text Reuse')
        self.SDST.setLevel(logging.INFO)
        # Add the log message handler to the logger
        handler = logging.handlers.RotatingFileHandler(log_filename,
                                                       maxBytes=1024 * 300,
                                                       backupCount=5)
        self.SDST.addHandler(handler)

    def log(self, message, timing=True):
        if timing:
            self.SDST.info(message + " ; " + str(datetime.datetime.now()))
        else:
            self.SDST.info(message)
        self.SDST.info("\n")

# disable logs 16.11.2021
# mlog = my_log()


def log_calls(func):
    # global mlog
    @wraps(func)
    def wrapper(*args, **kwargs):
        # mlog.log(func.__name__ + " Initiated args:".join(map(str,args)) )
        mlog.log(func.__name__ + " Initiated args: ".join(map(str,
                                                              args)), **kwargs)
        a = func(*args, **kwargs)
        mlog.log(func.__name__ + " Ended")
        return a
    return wrapper


def timing(func):
    """Utility decorator for measuring the time that took for a decorated function to run"""
    import time

    @wraps(func)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = func(*args, **kwargs)
        total_time = time.time() - t1
        # print(total_time)
        mlog.log("Info", 'Process {} Completed, Time: {} '.format(func.__name__,
                                                             total_time))
        return result

    return wrapper


class DB():

    @log_calls
    def __init__(self):
        db_type = None

    def set_local_file(self,file_name):
        self.local_file = self.local_path + file_name
        f = open(self.local_file, self.db_write_param)
        f.close()

    def set_db(self,db_param):

        def create_index(index_name):
            settings = '''
                        {
                            "mappings": {
                                "test": {
                                    "properties": {
                                        "sentence": {
                                            "type": "text",
                                            "analyzer": "hebrew"
                                        },
                                        "plain_sentence": {
                                            "type": "text"
                                        }
                                    }
                                }
                            }
                        }
            '''

            fields = ['location', 'sentence','title','categories']
            self.eso.DeleteIndex(index_name)
            #self.eso.CreateIndex(index_name, settings=settings)
            self.eso.CreateIndex(index_name)

        try:
            self.db_type = db_param['Type']
            if self.db_type == "elasticsearch":
                from elasticsearch import Elasticsearch
                from elastic_extention import elasticE
                self.server = db_param['server']
                self.doc_index = db_param['index']
                self.doc_type = db_param['doc_type']
                self.es = Elasticsearch(self.server)
                self.eso = elasticE(self.es)
                if db_param['index_type'] == "new":
                    create_index(self.doc_index)
                    self.next_doc_id = 1
                else:
                    # Move tp elastic_extention
                    res = self.es.search(index=self.doc_index, body={"aggs": {"max_id": {"max": {"field": "id"}}}})
                    self.next_doc_id = int(res['hits']['hits'][0]['_id']) + 1

            elif self.db_type == "inverted_index":
                self.inverted_index = {}
                self.documents = {}
            else:
                self.db_write_param = db_param['write']
                self.local_path = db_param['local_path']
                if db_param['file'] != "":
                    self.set_local_file(db_param['file'])
        except:
            raise ValueError("Data Base parameters Error")

    def fcreate(self,dict):
        if self.db_type == "elasticsearch":
            self.eso.AddDocument(index=self.doc_index,doc_type=self.doc_type,doc_id=self.next_doc_id,new_doc=dict)
            self.next_doc_id += 1
        elif self.db_type == "inverted_index":
            doc_id = len(self.documents)
            self.documents[doc_id] = {"location": dict["location"],
                                      "title": dict["title"],
                                      "categories": str(dict["categories"]),
                                      "versions": dict["versions"],
                                      "versionSource": str(dict["versionSource"]),
                                      "he_title": dict["he_title"]}
            for word_pos, word in enumerate(ftokens(dict['sentence'])):
                lemma = lemmatize(word)
                self.inverted_index.setdefault(lemma, []).append((doc_id, word_pos))
                for syn in synonyms(lemma):
                    self.inverted_index.setdefault(syn, []).append((doc_id, word_pos))
                for trans in translations(lemma):
                    self.inverted_index.setdefault(trans, []).append((doc_id, word_pos))

        else:
            import json
            with open(self.local_file, "a") as fp:
                json.dump(dict, fp, ensure_ascii=False)
                fp.write("\r\n")


class Sefaria():

    def __init__(self, db=None, nikud=False):
        if db:
            self.db = db
        self.nikud = nikud
        self.HTML = False
        self.leave_references = False
        self.window = 0
        self.slide = 0
        self.create_lexicons = True
        self.abbriviations = {}
        self.lexicons = {}
        self.object_operations = {'crete_text':True,
                                  'create_lexicon': False}

    def clean_special_chars(self, src):
        if src:
            src = src.replace("׳", "'")
            src = src.replace('״','"')
            for sym in ['־' , ":" , "׃"]:
                src = src.replace(sym, " ")
        return src

    def remove_nikud(self, src):
        '''
        If the inner variable nikud is set to True, the function
        returns the text with no Nikud
        :param src: The text to remove Nukud
        :return: the text with no Nikud.
        '''
        if self.nikud:
            return src
        else:
            try:
                normalized = unicodedata.normalize('NFKD', src)
                flattened = ''.join([c for c in normalized if not unicodedata.combining(c)])
                return flattened
            except:
                return ""

    def remove_html(self, src):
        if self.HTML:
            return src
        p = re.compile(r'<.*?>')
        return p.sub('', src)

    def remove_references(self,src):
        if self.leave_references:
            return src
        '''

        '''
        books = '''בראשית|שמות|ויקרא|במדבר|דברים|יהושע|שופטים|שמואל|מלכים|ישעיהו|ירמיהו|יחזקאל|הושע|יואל|עמוס|עובדיה|יונה|מיכה|נחום|חבקוק|צפניה|חגי|זכריה|מלאכי|תהלים|משלי|איוב|שיר השירים|רות|איכה|קהלת|אסתר|דניאל|עזרא|נחמיה|דברי הימים|ישעיה|ירמיה|תהילים|שה\\"ש|שה\"ש'''
        p = re.compile('\(([ ]*('+books+').*?)\)')
        cleaned_src = p.sub('', src)
        references = [self.clean_special_chars(x.group()).replace("'","").replace('"','') for x in re.finditer('\(([ ]*('+books+').*?)\)', src) ]
        return cleaned_src, references

    def parse_tokens(self,token_list,categories,location):
        '''
        Update the corpus lexticons and the corpus abbriviations (object vars)
        :param token_list: list of tokens from ftoken
        :return:
        '''

        if not self.create_lexicons:
            return

        corpora = ['Yerushalmi', 'Mishnah', 'Aggadic Midrash', 'Bavli',
                    'Torah', 'Prophets','Writings']
        exclude_corpus = ["Commentary"]
        # If the toekn ar efrom text to exclude, return
        if len(list(set(categories) & set(exclude_corpus))) > 0:
            return
        # Add token caculation for each relevant disctionary
        for lexicon in list(set(categories) & set(corpora)):
            tmp_lex = self.lexicons.get(lexicon,{})
            for token in token_list:
                tmp_lex[token] = tmp_lex.get(token,0) + 1
            self.lexicons[lexicon] = tmp_lex
        for token in token_list:
            # Add to abbriviation dict if neccessary
            if token.find('"') > -1 or token.find("'") > -1:
                abbs = self.abbriviations.get(token,[])
                abbs.append(location)
                self.abbriviations[token] = abbs
            tmp_lex = self.lexicons.get('All', {})
            tmp_lex[token] = tmp_lex.get(token, 0) + 1
            self.lexicons['All'] = tmp_lex

    def windows(self, tokens, return_type='dict', window=None, slide=None):
        if window:
            self.window = window
        if slide:
            self.slide = slide
        if self.window == 0:
            return [{"text": " ".join(tokens), "from_word": 1, "to_word": len(tokens), "text_length": len(tokens)}]
        #tokens = ftokens(data)
        start = 0
        windows = []

        while start <= len(tokens) - self.window:
            if return_type == 'dict':
                windows.append(
                    {"text": " ".join(tokens[start:start + self.window]), "from_word": start + 1, "to_word": start + self.window, "length": len(tokens)})
            else:
                windows.append(tuple(tokens[start:start + self.window]))
            if start + self.slide + self.window > len(tokens) and start + self.window < len(tokens):
                start = len(tokens) - self.window
            else:
                start += self.slide
        return windows

    def fprocess(self,text, path):
        '''
        Add the JSON leafs to the database (File or Elastic).
        If the JSON is nested one, it accumulate the path to add the fixed place of the sentence to the DB
        :param text: Nested JSON to parse
        :param path: The current position of the "sub" JSON, in the Big JASON (The entire book)
        :return: None
        '''
        if not text:
            return
        elif type(text) == list:
            for ix, sub in enumerate(text):
                self.fprocess(sub, path + "-" + str(ix + 1))
        else:
            sentence = self.remove_html(text)

            clean_sentence = self.clean_special_chars(sentence)
            clean_sentence = self.remove_nikud(clean_sentence)
            clean_sentence, references = self.remove_references(clean_sentence)

            # window = 0 , slide=0 means no sliding windows
            categories = self.categories.copy()
            categories.append(self.title)
            tokens_data = ftokens(clean_sentence)
            for win in self.windows(tokens=tokens_data ,return_type='dict'):
                if self.object_operations['crete_text']:
                    self.db.fcreate(dict={"location":path,
                                          "sentence":win['text'],
                                          "orig_sentence": sentence,
                                          "from_word": win['from_word'],
                                          "to_word" : win['to_word'],
                                          "text_length": win['text_length'],
                                          "title":self.title,
                                          "categories": categories,
                                          "references": references,
                                          "versions":win.get('versions',""),
                                          "versionSource":win.get('versionSource',""),
                                          "he_title":win.get('heTitle',"")})
                if self.object_operations['create_lexicon']:
                    self.parse_tokens(tokens_data, categories,path)

    def parse_sources(self,source,book):
        if type(source) == list:
            # print(book, len(source))
            if len(source) > 0 and len(source[0]) == 0:
                self.fprocess(source[1:], book)
            else:
                self.fprocess(source, book)
        else:
            for key in source:
                if key == "":
                    self.parse_sources(source[key], book)
                    # print(book , len(source[key]))
                else:
                    self.parse_sources(source[key], book+"-"+key)
                    # print(book, len(source[key]))

    def parse_library(self,path, book):
        '''
        Travel on a tree of files and directories.
        If it is a file named merged.json, it added this JSON
              to the database by calling the fprocess function with this json.
        :param path: The root tree to start scanning.
        :param book: The path from the Initial Root to the current sub root (A recursive function)
        :return: None
        '''
        for i in os.listdir(path):
            if i not in self.exclude:
                if os.path.isdir(path + i):
                    if i == "Hebrew":
                        self.parse_library(path + i + "/", book)
                    else:
                        self.parse_library(path + i + "/", book + i + "-")
                else:
                    if i == "merged.json":
                        with io.open(path + i, mode="r", encoding="utf-8") as myfile:
                            json_data = myfile.read()
                        data = json.loads(json_data)
                        self.title = data['title']
                        self.categories = data['categories']
                        if self.db.db_type == 'local_file':
                            self.db.set_local_file(book + '.json')

                        self.parse_sources(data['text'],book)

    def parse_libraries(self,collections,exclude,db_param,limit=0):
        '''
        Go thru several trees, for each, parse the tree by calling parse library.
        :param collections: A list of names of sub trees
        :param exclude: A list of directories of files not to parse
        :param db_param: A dictionary contain parameters to the database
        :param limit: If set to positive number greater than 0, limits the process for debug
        :return: None
        '''
        self.exclude = exclude
        self.db = DB()  # Create database object - either text file of Elasticsearch
        self.db.set_db(db_param)
        self.window = db_param['window']
        self.slide = db_param['slide']
        self.HTML = db_param['HTML']
        self.nikud = db_param['Nikud']
        if 'leave_references' in db_param:
            self.leave_references = db_param['leave_references']
        if 'object_operations' in db_param:
            self.object_operations = db_param['object_operations']

        # start = "Sefaria-Export-master/json/"
        start = "../source/Sefaria-Export-master/json/"
        for col in collections:
            self.parse_library(start + col + "/", col + "-")
        if self.object_operations['create_lexicon']==True:
            df_lexicons = {}
            for lex in self.lexicons.keys():
                df_tmp = pd.DataFrame.from_dict(self.lexicons[lex], orient='index', columns=["count"])
                total_words = float(sum(df_tmp['count']))
                df_tmp['P'] = df_tmp['count'] / total_words
                df_tmp = df_tmp.reset_index()
                df_tmp = df_tmp.sort_values(by=['count'], ascending=False)
                df_lexicons[lex] = df_tmp
            with open('df_lexicons.pickle', 'wb') as handle:
                pickle.dump(df_lexicons, handle)


class TanhumaPassage():

    def __init__(self):
        # self.parashot = pd.read_excel("/Users/hadarmiller/Dropbox/HaifaU/10_Text_Reuse/Data Bases And Systems/Source/local/Parashat_Hashavua.xlsx")
        #s4 = '/Users/hadarmiller/Dropbox/HaifaU/10_Text_Reuse/Data Bases And Systems/Source/Sefaria-Export-master/json/Midrash/Aggadic Midrash/Midrash Tanchuma/Hebrew/merged.json'
        #with io.open(s4, mode="r", encoding="utf-8") as myfile:
        #    json_data = myfile.read()

        # self.tanhuma = json.loads(json_data)
        self.parashot = None

        self.sf = Sefaria(nikud=False)
        self.es = Elasticsearch(settings.ELASTICSEARCH_DSL['default']['hosts'])
        self.es_index = settings.TEXT_REUSE['es_index']
        self.eso = elasticE(self.es)
        self.unit = self.unit_tokens =  self.uparts =  self.cites = self.unit_parallels = None
        self.win = self.slide = None
        self.target_json = 'parallel_site/parallels.json'
        self.small_word_size = 2
        self.api_mode = False
        self.system_status = "OK"
        self.system_checks = self._check_infrastructure()

    def _check_infrastructure(self):
        indices = self.es.indices.get_alias("*")
        if self.es_index not in indices:
            print(indices)
            self.system_status = 'Index: {} is missing available indices: {}'.format(self.es_index, indices)
            return False
        return True


    def extract_cites(self, text, remove=True):
        ret = []
        cites = re.findall("\(([^)]+)\)", text)

        if remove:
            p = re.compile(r"\(([^)]+)\)")
            ret = (p.sub('', text), cites)
        else:
            ret = (text, cites)
        return ret

    def prepare_suspect_unit(self, unit, remove_cites=False):
        '''
            remove Nukid and HTML from the suspect unit
            Split the unit into tokens and wondows according to the parameters
        '''
        unit = self.sf.remove_nikud(unit)
        unit = self.sf.remove_html(unit)
        unit = self.sf.clean_special_chars(unit)
        unit = unit.replace("\u200e", "")
        unit, cites = self.sf.remove_references(unit)
        unit_tokens = ftokens(unit)
        uparts = self.sf.windows(unit_tokens, window=self.win, slide=self.slide)
        return unit, unit_tokens, uparts, cites

    def query_builder(self, text, fields=["sentence"], fuzziness=0, exact_match=True, size=20, slop=0, query_type="span",operator="AND"):
        if query_type == 'query_string':
            q = text
            if fuzziness != 0:
                fuzz = "~" + str(fuzziness) + " "
                q = fuzz.join(ftokens(text)) + fuzz
            if exact_match:
                q = '"' + q + '"'
            query = {"query_string": {"fields": fields,
                                      "default_operator": operator,
                                      "query": q}}
            # print(query)
        elif query_type == 'match':
            query = {"query": {"match": {fields[0]: {"query": text,
                                                     "fuzziness": "AUTO",
                                                     "operator": operator}}}}
        else:
            clauses = []
            for term in ftokens(text):
                new_span = {"span_multi": {"match": {"fuzzy": {fields[0]: {"fuzziness": fuzziness, "value": term}}}}}
                clauses.append(new_span)
            query = {"span_near": {"clauses": clauses, "slop": slop, "in_order": exact_match}}
        ret = {"size": size, "query": query}
        return ret

    def find_parallels(self, search_text, exact_order=True, size=10, fuzziness=0, slop=0, query_type="span",
                       operator='AND'):
        part_refs = {}
        st = clean_special_chars(search_text).replace('"',"").replace("'","")
        q = self.query_builder(text=st, fuzziness=fuzziness, exact_match=exact_order, size=size, slop=slop,
                          query_type=query_type, operator=operator)
        #print(str(q))
        result = self.es.search(index=self.es_index, body=q)
        if result.get('hits') is not None and result['hits'].get('hits') != []:

            for hit in result['hits']['hits']:
                part_refs[hit['_source']['location']] = {"Score": hit['_score'],
                                                         "Exact_Match": exact_order,
                                                         "fuzzy": fuzziness,
                                                         "_id": hit['_id'],
                                                         "search_text": search_text}
        return part_refs

    def merge_parallels(self, tested_part, part_refs, unit_parallels, u_tokens, query_type="span", fuzziness = 0):
        '''
            For each of matched documents to the susspect, manage a vector (array) with size of susspect length.
            The vectore = {0,1} where 1 represent that the i-th token in the susspect was found in the document.
            So when new windows are processed, The 1's in the matching array are accumulated
        '''
        from_word = tested_part['from_word']
        to_word = tested_part['to_word']
        # print(tested_part)
        # print(part_refs.keys())
        for ref in part_refs.keys():
            # print(part_refs[ref]['_id'])
            parallels = unit_parallels.get((ref, part_refs[ref]['_id']), np.zeros(len(u_tokens)))
            if query_type == 'span':
                parallels[from_word - 1:to_word] = 1
            else:
                compare = self.get_ref_part((ref, part_refs[ref]['_id']))
                compare = ftokens(compare)
                for idx, token in enumerate(u_tokens):
                    if token in compare:
                        parallels[idx] = 1
            unit_parallels[(ref, part_refs[ref]['_id'])] = parallels
        # unit_parallels[(tested_part['from_word'],tested_part['to_word'])] = part_refs
        return unit_parallels

    def scan_corpus(self,uparts, unit_tokens, corpus_index, result_size=1500, quer_type="span", operator="And", exact_order=True, fuzziness=0, slope=0):
        unit_parallels = {}
        unit_refs = {}
        for tested_part in uparts:
            # Search all parallels for one part
            part_refs = {}
            search_text = tested_part['text']
            # Find paralles in the same order and NO changes
            part_refs = self.find_parallels(search_text,
                                            exact_order=exact_order,
                                            fuzziness=fuzziness,
                                            slop=slope,
                                            size=result_size,
                                            query_type=quer_type,
                                            operator=operator)
            unit_parallels = self.merge_parallels(tested_part, part_refs, unit_parallels, u_tokens=unit_tokens,query_type=quer_type, fuzziness = fuzziness)
        return unit_parallels

    def classify_tanhumah(self,passage, cites, patterns):
        '''
            classify the Tanhuma type (Open with far cite , Open with question)
            according to a regular language developed for it.
            Supported vocabulary: Sentence, Cite, Gap.
        '''

        def single_cite_block(last_text_pos, pattern_recognition, pattern_prob, cites, pattern_name):
            # Locate cite the starts in the last_text_pos srounding
            cite_end_position = 0
            for c in cites:
                next_cite_pos = np.where(self.unit_parallels[c] == 1)[0]
                if len(next_cite_pos) > 0 and next_cite_pos[0] >= last_text_pos and next_cite_pos[
                    0] <= last_text_pos + gap + 2:
                    pattern_prob[block - 1:block] = 1.0 / len(pattern_prob)
                    # Move the text position to the end of the cite
                    last_text_pos = next_cite_pos[0]
                    for i in next_cite_pos[1:]:
                        if i == last_text_pos + 1:
                            last_text_pos = i
                        else:
                            break
                    tmp = pattern_recognition[pattern_name].get(block, {})
                    tmp['cite'] = c
                    pattern_recognition[pattern_name][block] = tmp
                    return (True, last_text_pos)

            return (False, last_text_pos)

        def gap_block(pattern, gap, passage, block, pattern_prob):
            if type(pattern['sequence'][block]) == str:
                gap = len(passage)
            else:
                gap = pattern['sequence'][block][1]
            pattern_prob[block - 1:block] = 1.0 / len(pattern_prob)
            return gap

        def sentence_block(pattern, last_text_pos, gap, pattern_prob, block, pattern_recognition, pattern_name, unit):

            words = pattern['sequence'][block][-1]
            for word in words:
                exists = passage.find(word)
                if exists > -1:
                    if len(unit[:exists].strip().split(" ")) <= last_text_pos + gap + 2:
                        # print("Found Contuinuity: {}".format(next_cite_pos))
                        pattern_prob[block - 1:block] = 1.0 / len(pattern_prob)
                    else:
                        # If the pattern was found but not in the sequence add 0.3 to the match probability
                        pattern_prob[block - 1:block] = (1.0 / len(pattern_prob)) * 0.3
                    tmp = pattern_recognition[pattern_name].get(block, {})
                    tmp[word] = exists
                    pattern_recognition[pattern_name][block] = tmp
                    # last_text_pos += len(word.split(' '))
                    last_text_pos = len(unit[:exists + len(word)].strip().split(" "))
                    return (True, last_text_pos)
            return (False, last_text_pos)

        pattern_recognition = {}
        best_estimation = 0
        for pattern in patterns:  # Loop over all available patterns and calculate probability for each

            last_text_pos = 0
            last_cite_pos = 0
            gap = 0  # Alowed gap between blockes

            pattern_name = pattern['name']
            pattern_prob = np.zeros(len(pattern['sequence']))
            pattern_recognition[pattern_name] = {}
            for block in sorted(pattern['sequence'].keys()):  # Loop over all blocks of the pattern
                if "gap" in pattern['sequence'][block]:
                    gap = gap_block(pattern, gap, passage, block, pattern_prob)
                elif "cite" in pattern['sequence'][block]:
                    if "single" in pattern['sequence'][block] and last_cite_pos < len(cites):
                        # print("last text pos: {} block: {}".format(last_text_pos,block))
                        res, last_text_pos = single_cite_block(last_text_pos, pattern_recognition, pattern_prob, cites,
                                                               pattern_name)
                        # print("last text pos: {}".format(last_text_pos))
                    else:
                        # Search for max 5 cites in a row
                        for i in range(5):
                            res, last_text_pos = single_cite_block(last_text_pos, pattern_recognition, pattern_prob,
                                                                   cites, pattern_name)
                            if not res:
                                break
                    gap = 0  # Reset gap as it is not the last block

                elif "sentence" in pattern['sequence'][block]:
                    # print("last text pos: {} block: {}".format(last_text_pos,block))
                    res, last_text_pos = sentence_block(pattern, last_text_pos, gap, pattern_prob, block,
                                                        pattern_recognition, pattern_name, self.unit)
                    gap = 0
                    # print("last text pos: {} ".format(last_text_pos))
            pattern_recognition[pattern_name]['probability'] = pattern_prob.sum()
            if pattern_prob.sum() > best_estimation:
                best_estimation = pattern_prob.sum()
                pattern_recognition['best_estimation_name'] = pattern_name
                pattern_recognition['best_estimation_probability'] = np.round(pattern_prob.sum(), 2)

        return pattern_recognition

    def get_tanhuma_part(self, parasha, iperek, ipasuk, res_size=20):
        '''
            Fetch the text from Elastic of a specific Tanhuma part
            like 'Tanchuma--Bereshit-1-1'
        '''
        perek = str(gematria_to_int(iperek))
        pasuk = str(gematria_to_int(ipasuk))
        b12 = '''{
                  "query": {
                    "match": {
                      "location": {
                        "query": "Tanchuma--''' + parasha + "-" + perek + "-" + pasuk + '''",
                        "operator": "and"
                      }
                    }
                  }
                }'''
        # print(b12)
        result = self.es.search(index=self.es_index, body=b12)
        if result.get('hits') is not None and result['hits'].get('hits') != []:
            return result['hits']['hits'][0]['_source']['sentence']
        return ""

    def get_ref_part(self, sp):
        '''
            Fetch the text from Elastic of a specific part
            like 'Midrash-Aggadic Midrash-Midrash Tanchuma--Bereshit-1-1'
        '''

        b12 = '{"query": {"terms": {"_id": [ "' + sp[1] + '" ]}}}'
        # print("\n\n{}\n\n".format(b12))
        result = self.es.search(index=self.es_index, body=b12)
        if result.get('hits') is not None and result['hits'].get('hits') != []:
            return result['hits']['hits'][0]['_source']['sentence']
        return ""

    def calc_target_matrix(self, source, win, slide, query_type="span",
                           query_string="", fuzziness=0, exact_order=True):
        '''
            Locate the source from the DB and mark all the words
        '''
        compare = self.get_ref_part(source)
        tunit, tunit_tokens, tuparts, tcites = self.prepare_suspect_unit(compare)
        compare_parallels = np.zeros(len(tunit_tokens))

        if query_type == 'query_string':
            tokens = ftokens(query_string)
            for token in tokens:
                if token in tunit_tokens:
                    compare_parallels[tunit_tokens.index(token)] = 1
            return tunit, tunit_tokens, compare_parallels

        # Tests
        # If the search is and exact match ( in order)
        # calculate the Levinstein distance and compare it to the number
        # of allowed mismatch letters according to the search criteria.
        # If the search is not in order, use FuzzyWuzzy to calculate the
        # ratio between the two strings ( partial ratio allow words disorder)

        import Levenshtein as lev
        from fuzzywuzzy import fuzz
        for suspect_win in self.uparts:
            for source_win in tuparts:
                if exact_order:
                    distance = lev.distance(suspect_win['text'],
                                            source_win['text'])
                    if distance <= win * fuzziness:
                        compare_parallels[source_win['from_word'] -1:source_win['to_word']] = 1
                else:
                    # The theshold should be calibrated - Hadar 23.1.2021
                    threshold = 95 - (fuzziness * 20)
                    partial_ratio = fuzz.token_set_ratio(suspect_win['text'],
                                                         source_win['text'])
                    if partial_ratio > threshold:
                        compare_parallels[source_win['from_word'] -1:source_win['to_word']] = 1
        return tunit, tunit_tokens, compare_parallels


    def display_parallel(self, tokens, parallel_table):
        def add_token(token, color, text):
            text += "<text style=color:{}>{}</text> ".format(color, token)
            return text

        html = ""
        # unit_parallels[source][inx]
        for inx, token in enumerate(tokens):
            if parallel_table[inx] > 0:
                html = add_token(token, 'green', html)
            else:
                html = add_token(token, 'red', html)
        return html


    def _filter_text_reuse_detection(self, compare_paralles,source, tunit_tokens):
        '''
        Filter the prallel candidates when the parallel strings are signficant.
        This is a rule based decision. A vocabulary of terms are generated to allow
        dynamic rules generation. The terms are: consecutive_match, assage_length, query_length
        :param compare_paralles:
        :param tunit_tokens:
        :return:
        '''
        # def ones_runs(a):
        #     # Create an array that is 1 where a is greater that 0,
        #     # and pad each end with an extra 0.
        #     iszero = np.concatenate(([0], np.greater(a, 0).view(np.int8), [0]))
        #     absdiff = np.abs(np.diff(iszero))
        #     # Runs start and end where absdiff is 1.
        #     ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
        #     return ranges

        def check_rule(rule_name, rule_content,source, compare_paralles, cons):
            consecutive_match = cons[1] - cons[0]
            short_words = len([x for x in tunit_tokens[cons[1]:cons[0]] if len(x)<=self.small_word_size])
            consecutive_match_likelihood = 0 #**** TO COMPELTE *****
            query_length = len(self.unit_tokens)
            passage_length = len(compare_paralles)
            try:
                rule_expression = eval(rule_content["expression"])
                rule_corpus = rule_content["corpus"]
                if rule_expression:
                    if rule_corpus == "any" or source[0][:len(rule_corpus)] == rule_corpus:
                        ret = {"rule": rule_name, "result": True}
                    else:
                        ret = {"rule": rule_name, "result": False}
                else:
                    ret = {"rule": rule_name, "result": False}
            except:
                ret = {"rule": "Error parsing rule: {}: {}".format(rule_name, rule_content["expression"] ), "result": False}
            return ret


        if self.text_reuse_rules is None:
            return False, "No text Reuse rules exist"

        # Verbatim Text reuse
        # A sequence of words in the same order
        # No additions or deletions
        # Orthographical changes may apear. ( fuzziness? )
        #

        #consecuteives = ones_runs(compare_paralles)
        consecuteives = ones_sequence(compare_paralles)
        result = False
        reuses = []

        for cons in consecuteives:
            explains = []
            # OR condition between the groups
            for or_rule_name, or_rule_content in self.text_reuse_rules.items():
                text_reuse_indicator = True
                # AND cindition within each group
                for rule_name, rule_content in or_rule_content.items():
                    rule_result = check_rule(rule_name,rule_content, source, compare_paralles, cons)
                    explains.append((or_rule_name,rule_result))
                    if not rule_result['result']:
                        text_reuse_indicator = False
                        break
                if text_reuse_indicator:
                    reuses.append(
                        {"location": cons, "result": text_reuse_indicator, "explain": explains, "source": source})
                    break
            reuses.append({"location": cons, "result": text_reuse_indicator, "explain": explains, "source": source})
            if text_reuse_indicator:
                result = True

        return result, reuses

    def prepare_passage_to_site(self, output_file, corpus_scope, source_name,
                                source_type=None, query_type="span",
                                query_string="",
                                exact_order=True):
        # ---------------------------
        # Create dictionary which the keys are the index of the word in the sentence
        # and the values are all the cites founded by the framework for this word
        # ---------------------------
        unit_cross_reference = {}

        # For all the relelvant documents founded for the suspect
        # For each token in the suspect unit prepare a list of
        # passages that relate to it. Unit_cross_reference
        self.parsed_sources = {}
        for ref in corpus_scope:
            # For each token in the suspect found in a specific document
            for cross_word in np.argwhere(self.unit_parallels[ref] > 0):
                temp = unit_cross_reference.get(cross_word[0],
                                                {"word": self.unit_tokens[cross_word[0]], "count": 0, "references": []})
                temp['count'] += 1
                temp['references'].append(ref)
                unit_cross_reference[int(cross_word[0])] = temp
                # unit_cross_reference

        # ---------------------------
        # Convert the unit_cross_reference dictionary to a JSON
        # in the format required by the parallel site
        # ---------------------------
        links_refs = []
        short_links_refs = []
        tr_links_refs = []
        short_tr_links_refs = []
        for i in unit_cross_reference:
            tmp = {"id": i}
            tr_tmp = {"id": i}
            refs = []
            tr_refs = []
            short_refs = []
            short_tr_refs = []
            for source in unit_cross_reference[i]['references']:
                if source in self.parsed_sources:
                    tunit, tunit_tokens, compare_parallels, reuse_result, reuse_explain, to_site = self.parsed_sources[source]

                else:

                    tunit, tunit_tokens, compare_parallels = self.calc_target_matrix(source=source, win=self.win,
                                                                                     slide=self.slide,
                                                                                     query_type=query_type,
                                                                                     query_string=query_string,
                                                                                     fuzziness=self.fuzziness,
                                                                                     exact_order=exact_order)
                    reuse_result, reuse_explain = self._filter_text_reuse_detection(compare_parallels, source,tunit_tokens)
                    self.parsed_sources[source] = (tunit, tunit_tokens, compare_parallels,reuse_result, reuse_explain)

                    to_site = {"target_parallel_view" : self.display_parallel(tunit_tokens, compare_parallels),
                               "source_parallel_view": self.display_parallel(self.unit_tokens, self.unit_parallels[source])}

                    self.parsed_sources[source] = (tunit, tunit_tokens, compare_parallels, reuse_result,
                                                   reuse_explain, to_site)
                short_new_ref = {"source": source[0],
                                 "count": sum(self.unit_parallels[source]),
                                 "source_id": source[1],
                                 "source_link": "www.google.com",
                                 # "suspect_matrix": str(self.unit_parallels[source]),
                                 # "source_matrix": str(compare_parallels),
                                 # "text_reuse_explain": str(reuse_explain),
                                 "text_reuse_result": reuse_result
                             }
                new_ref = short_new_ref.copy()
                new_ref["suspect_matrix"] = str(self.unit_parallels[source])
                new_ref["source_matrix"] = str(compare_parallels)
                new_ref["target_parallel_view"] = to_site["target_parallel_view"]
                new_ref["source_parallel_view"] = to_site["source_parallel_view"]
                new_ref["text_reuse_explain"] = str(reuse_explain)
                short_refs.append(short_new_ref)
                refs.append(new_ref)

                if reuse_result:
                    short_tr_refs.append(short_new_ref)
                    tr_refs.append(new_ref)
            tmp['sources'] = sorted(refs, key=lambda tup: tup['count'],
                                    reverse=True)
            links_refs.append(tmp)
            tr_tmp['sources'] = sorted(tr_refs, key=lambda tup: tup['count'],
                                       reverse=True)
            tr_links_refs.append(tr_tmp)
            for ldic, rdic in [(short_refs, short_links_refs),
                               (short_tr_refs, short_tr_links_refs)]:
                tmp = {"id": i}
                tmp['sources'] = sorted(ldic, key=lambda tup: tup['count'],
                                         reverse=True)
                rdic.append(tmp)

        json_data = {"source text": ' '.join(self.unit_tokens),
                     "source name": source_name,
                     "links": links_refs,
                     "tr_links": tr_links_refs,
                     "short_links": short_links_refs,
                     "short_tr_links": short_tr_links_refs}
        if source_type:
            if 'best_estimation_name' in source_type:
                json_data["source_type"] = source_type['best_estimation_name']
            else:
                json_data["source_type"] = "סווג לא נמצא"
            if 'best_estimation_probability' in source_type:
                json_data["source_type_prob"] = source_type['best_estimation_probability']
            else:
                json_data["source_type_prob"] = -1
        # Print the json for tests
        # app_json = json.dumps(json_data, ensure_ascii=False).encode('utf8')
        # pprint.pprint(app_json.decode())

        # ---------------------------
        # Store result in a parallel.json file
        # in the format required by the parallel site
        # ---------------------------
        if output_file == "None":
            return json_data
        else:
            with open(output_file, 'w', encoding='utf8') as outfile:
                json.dump(json_data, outfile, ensure_ascii=False)
            return "File Create"




    def _process(self, corpus, source_name, suspect_unit, exact_order=True,  query_type='span',operator="And",
                 scan_corpus = True):
        if scan_corpus:
            self.unit, self.unit_tokens, self.uparts, self.cites = self.prepare_suspect_unit(suspect_unit,
                                                                                         remove_cites=False)
            self.unit_parallels = self.scan_corpus(uparts=self.uparts,
                                               unit_tokens=self.unit_tokens,
                                               exact_order=exact_order,
                                               fuzziness=self.fuzziness,
                                               slope=self.slope,
                                               quer_type=query_type,
                                               operator=operator,
                                               corpus_index=self.es_index)
        cites_from_torah, corpus_scope = self._filter_corpus_candidates(corpus)
        pattern1 = {'name': 'פתיחתא בפסוק רחוק',
                    'sequence': {
                        1: ("single", "cite"),
                        2: ("sentence", ['זה שאמר הכתוב', 'זש"ה', 'שנאמר']),
                        3: ("single", "cite"),
                    }
                    }
        pattern2 = {'name': 'פתיחתא בשאלה',
                    'sequence': {
                        1: ("single", "cite"),
                        2: ("sentence", ["ילמדנו רבנו", 'ילמדנו רבינו']),
                        3: ("gap"),
                        4: ("sentence", ["כך שנו רבותנו", "כך שנו רבותינו"])
                    }
                    }
        tanhum_type = self.classify_tanhumah(self.unit, cites_from_torah, [pattern1, pattern2])
        refs_data = self.prepare_passage_to_site(self.target_json, source_name=source_name,
                                           corpus_scope=corpus_scope,
                                           source_type=tanhum_type,
                                           query_type=query_type,
                                           query_string=suspect_unit,
                                           exact_order=exact_order)

        return refs_data

    def _filter_corpus_candidates(self, corpus):
        cites_from_torah = list(set([x for x in self.unit_parallels.keys() if x[0][:12] == "Tanakh-Torah"]))
        corpora = {"torah": ["Tanakh-Torah"],
                   "writings": ["Tanakh-Writings"],
                   "prophets": ['Tanakh-Prophets'],
                   "tanach": ["Tanakh-Torah", "Tanakh-Writings",
                              'Tanakh-Prophets'],
                   "tanhumah": ['Midrash-Aggadic Midrash-Midrash Tanchuma'],
                   "mishnah": ['Mishnah'],
                   "tosefta": ['Tanaitic-Tosefta'],
                   "masachtot_ktanot": ['Tractate'],
                   "halakhah": ['Halakhah'],
                   "kabbalah": ['Kabbalah'],
                   "midrash-halachic": ['Midrash-Halachic'],
                   "bavli": ["Talmud-Bavli-Seder"],
                   "yerushalmi": ["Talmud-Yerushalmi-Seder"],
                   "aggadic": ["Midrash-Aggadic"]}
        corpus_scope = []
        if 'commentary' in corpus:
            exclude_commentary = False
            corpus.remove('commentary')
        else:
            exclude_commentary = True
        # print(corpus)
        if 'ALL' in corpus:
            corpus_scope = self.unit_parallels
        else:
            for book in corpus:
                pref = []
                for pref in corpora[book.lower()]:
                    if exclude_commentary:
                        corpus_scope += [x for x in self.unit_parallels.keys() if
                                         x[0][:len(pref)] == pref and x[0].find('Commentary') < 0]
                    else:
                        corpus_scope += [x for x in self.unit_parallels.keys() if x[0][:len(pref)] == pref]
            corpus_scope = list(set(corpus_scope))
        return cites_from_torah, corpus_scope

    def process_passage(self, suspect_unit, inspect_parasha, inspect_chapter,
                        win=3, slide=1, output=None, corpus='Tanach',
                        exact_order=True, fuzziness=0, slope=0,
                        query_type='span', operator="And", text_reuse_rules = None):
        self.win = win
        self.slide = slide
        self.fuzziness = fuzziness
        self.slope = slope
        if not text_reuse_rules:
            self.text_reuse_rules = get_text_reuse_rules()
        else:
            self.text_reuse_rules = text_reuse_rules
        if output:
            self.target_json = output
        source_name = "תנחומא: {} , {}".format(inspect_parasha,
                                               inspect_chapter)
        # parasha_sefaria = self.parashot[self.parashot['Parasha'] == inspect_parasha]['Parasha_Sefaria'].iloc[0]
        # suspect_unit = self.tanhuma['text'][parasha_sefaria][gematria_to_int(inspect_chapter) - 1][0]
        ret = self._process(corpus, source_name, suspect_unit,
                            exact_order=exact_order, query_type=query_type, operator=operator)
        return ret


    def process_text(self, suspect_unit, win=3, slide=1, output=None,
                     corpus='Tanach', exact_order=True, fuzziness=0,
                     slope=0, query_type='span', operator="And",
                     tanhuma_source = None, text_reuse_rules = None, scan_corpus = True):
        self.win = win
        self.slide = slide
        self.fuzziness = fuzziness
        self.slope = slope
        if text_reuse_rules:
            self.text_reuse_rules = get_text_reuse_rules(text_reuse_rules)
        else:
            self.text_reuse_rules = get_text_reuse_rules()
        if output:
            self.target_json = output
        if tanhuma_source:
            source_name = "תנחומא: {} , {}, {}".format(tanhuma_source[0],
                                                   tanhuma_source[1],
                                                   tanhuma_source[2])
        else:
            source_name = str(corpus)
        ret = self._process(corpus, source_name, suspect_unit,
                            exact_order=exact_order, query_type=query_type, operator=operator,
                            scan_corpus=scan_corpus)

        return ret




    def restore_corpus_scan(self, unit, unit_parallels, uparts, unit_tokens):
        self.unit_parallels = unit_parallels
        self.unit_tokens = unit_tokens
        self.uparts = uparts
        self.unit = unit
        self.target_json = "None"
        # Need to store the references when on the first execution
        self.cites = []

        return


class GenerateData():
    def __init__(self):

        self._clear_local_data()
        self.df_corpora_locations = pd.read_pickle("df_corpora_locations.pickle")
        with open('df_lexicons.pickle', 'rb') as handle:
            self.lexicons = pickle.load(handle)

    def _clear_local_data(self):
        self.df_synthetic_reuse_links = pd.DataFrame(columns=['case_id',"text", "source", "source_text", "reuse_from_source",
                                            "ofset","source_matrix","part_suspect_matrix","reuse_size"])
        self.df_synthetic_text = pd.DataFrame(columns=['case_id',"text","number_of_reuses","matrix"])

    def _get_source(self, selected_source):
        q = '''
        {
          "query": {
            "match": {
              "location": "'''+selected_source+'''"

            }
          }
        }
        '''
        result = tp.es.search(index="rabbinic", body=q)

        if 'hits' in result:
            for hit in result['hits']['hits']:
                if hit['_source']['location'] == selected_source:
                    return hit['_source']['sentence']
            return result['hits']['hits'][0]['_source']['sentence']
        return ""

    def _drawPLSample(self,alpha=2.5,Xmin=1,n=1,xtype='trunc',Xmax = None):
        expt = 1.0/(1.0-alpha)
        ysamples = np.random.random(n)
        sample = Xmin*ysamples**expt
        if Xmax is not None:
            sample=sample[sample<Xmax]
        if xtype == 'trunc':
            sample = [float(int(x)) for x in sample]
        return sample

    def _drawUniform(self,xmin=0,xmax=1,n=1,xtype='trunc'):
        s = np.random.uniform(xmin,xmax,n)
        if xtype == 'trunc':
            s = [int(x) for x in s]
        return s

    def _drawUniformBinary(self,n=1,prob=0.5):
        s = np.random.uniform(0,1,n)
        s[s>(1-prob)]=1
        s[s<=(1-prob)]=0
        return [int(x) for x in s]

    def create_synthetic_text(self, text_reuse_prob, long_tr_probability):

        def single_text_reuse():

            # choose randomaly if the reuse is long or short reuse
            is_long_tr = self._drawUniformBinary(n=1, prob=long_tr_probability)[0]
            # choose randomaly the length of reuse
            if is_long_tr:
                text_reuse_size = np.random.choice(np.arange(15, 25), p=[0.1]*10)
            else:
                text_reuse_size = np.random.choice(np.arange(3, 9), p=[0.3, 0.3, 0.1, 0.1, 0.1, 0.1])

            # choose randomaly the passage to reuse according to the required tr length
            # If therequired length is less than 5, choose from the Tanakh only.
            if text_reuse_size < 5:
                tmp_record = self.df_corpora_locations[(self.df_corpora_locations['text_length'] >= text_reuse_size)&((self.df_corpora_locations['corpus'] == 'Tanakh-Torah')|(self.df_corpora_locations['corpus'] == 'Tanakh-Writings')|(self.df_corpora_locations['corpus'] == 'Tanakh-Prophets'))].sample(n=1)
            else:
                tmp_record = self.df_corpora_locations[self.df_corpora_locations['text_length'] >= text_reuse_size].sample(n=1)
            tmp_text = tmp_record['text'].iloc[0]
            tmp_length = tmp_record['text_length'].iloc[0]
            tmp_location = tmp_record['location'].iloc[0]
            tmp_corpus = tmp_record['corpus'].iloc[0]

            # randomaly select the start point to reuse in the tmp passage
            start = self._drawUniform(0,tmp_length-text_reuse_size+1,1)[0]
            tmp_text_tokens = ftokens(tmp_text)
            tmp_reuse_matrix = np.zeros(len(tmp_text_tokens))
            tmp_reuse_matrix[start:start+text_reuse_size] = 1
            tmp_reuse_part = " ".join(tmp_text_tokens[start:start+text_reuse_size])

            # Add prefix and sufix to the reused text
            prefix_length= self._drawUniform(0,10)[0]
            sufix_length = self._drawUniform(0,10)[0]
            corpus_to_lex = {'Talmud-Bavli': "Bavli", 'Midrash-Aggadic':"Aggadic Midrash", 'Tanakh-Prophets':'Prophets',
                             'Tanakh-Writings':'Writings', 'Tanakh-Torah':'Torah', 'Mishnah':'Mishnah', 'Talmud-Yerushalmi':'Yerushalmi'}
            corpus_lexicon = self.lexicons[corpus_to_lex[tmp_corpus]]
            tmp_prefix = " ".join(list(corpus_lexicon.sample(prefix_length,
                                                       weights="P",replace=True)["index"]))
            tmp_sufix = " ".join(list(corpus_lexicon.sample(sufix_length,
                                                       weights="P",replace=True)["index"]))
            tmp_reuse_passage = tmp_prefix + " "+tmp_reuse_part + " "+tmp_sufix+" "
            tmp_suspect_matrix = np.concatenate((np.array([0]*prefix_length),
                                                 np.array([1]*text_reuse_size),
                                                 np.array([0]*sufix_length)))

            return {"text": tmp_reuse_passage,
                    "source": tmp_location,
                    "source_text": tmp_text,
                    "reuse_from_source": tmp_reuse_part,
                    "ofset": start +1,
                    "source_matrix": tmp_reuse_matrix,
                    "part_suspect_matrix": tmp_suspect_matrix,
                    "reuse_size": text_reuse_size}

        # select randomaly if the current text has textreuse
        has_text_reuse = self._drawUniformBinary(n=1,prob=text_reuse_prob)[0]
        reuse_links = pd.DataFrame(columns=["text", "source", "source_text", "reuse_from_source",
                                                "ofset","source_matrix","part_suspect_matrix","reuse_size"])
        df_reuse_text = pd.DataFrame(columns=["text","number_of_reuses","matrix"])
        if has_text_reuse:
            number_of_tr = self._drawPLSample()[0]
            new_text = ""
            new_text_matrix = np.array([])
            counter = 1
            for reuse in range(int(number_of_tr)):
                next_tr = single_text_reuse()
                next_tr['reuse_index'] = counter
                reuse_links = reuse_links.append(next_tr,ignore_index=True)
                new_text += next_tr['text']
                new_text_matrix = np.concatenate((new_text_matrix, next_tr['part_suspect_matrix']*counter))
                counter += 1
            df_reuse_text = df_reuse_text.append({"text":new_text,"number_of_reuses": number_of_tr,"matrix": new_text_matrix}, ignore_index=True)

        else:
            no_tr_passage_size = np.random.choice(np.arange(5, 20), p=[1/15]*15)
            corpus_lexicon = self.lexicons['All']
            random_tokens = list(corpus_lexicon.sample(no_tr_passage_size,
                                                           weights="P",replace=True)["index"])
            new_text = " ".join(random_tokens)
            df_reuse_text = df_reuse_text.append({"text":new_text,
                                                  "number_of_reuses": 0,"matrix": np.zeros(len(random_tokens))},
                                                 ignore_index=True)
        return reuse_links, df_reuse_text

    def store_data(self, name, con=None):

        def _insert_postgre_rows(table, fields, values, connection ):
            new_id = 0
            conn = None
            connect_string = 'dbname=' + connection['database'] + " user=" + connection['user'] +' password=' + connection['password']
            connect_string += " host=" + connection['host'] + " port=" + connection['port'] # +" sslmode='require'"
            try:
                conn = psycopg2.connect(connect_string)
                cur = conn.cursor()
                if type(values)== list:
                    values_num = '%s,' * (len(values[0])-1) + "%s"
                    sql = "INSERT INTO " + table + fields +" VALUES("+ values_num+ ") ;"
                    cur.executemany(sql,values)
                else:
                    values_num = '%s,' * (len(values)-1) + "%s"
                    sql = "INSERT INTO " + table + fields +" VALUES("+ values_num+ ") RETURNING id;"
                    cur.execute(sql, values)
                    new_id = cur.fetchone()[0]
                conn.commit()
                cur.close()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            finally:
                if conn is not None:
                    conn.close()
            return new_id

        if con:
            # connection = {'database':'crowdsourcing','user':'hadar',
            #               'password':'1miller2',
            #               'host': '127.0.0.1','port': '5442'}
            sql = 'postgresql://' + con['user'] + ":" + con['password'] + "@" + con['host'] + ":" + con['port'] + "/" + con['database']
            engine = create_engine(sql)

            # Store all cases
            data = []
            case_trans = {}
            res = engine.execute(" select max(case_id) from textreuse_syntheticcase")
            for i in res:
                case_id = i[0]
                break
            if case_id:
                case_id += 1
            else:
                case_id = 1
            for row in range(len(self.df_synthetic_text)):
                case_id += 1
                case_trans[self.df_synthetic_text['case_id'].iloc[row]] = case_id
                data.append((case_id,
                             name,
                             self.df_synthetic_text['text'].iloc[row],
                             self.df_synthetic_text['number_of_reuses'].iloc[row],
                             str(self.df_synthetic_text['matrix'].iloc[row])))
            table = 'textreuse_syntheticcase'
            fields = "(case_id,batch_name,text,number_of_reuses,matrix)"
            _insert_postgre_rows(table=table, fields=fields,
                                 values=data[:], connection=con)

            # Store all link_set
            data = []
            res = engine.execute(" select max(case_id) from textreuse_syntheticcaselinks")
            for i in res:
                reuse_index = i[0]
                break
            if reuse_index:
                reuse_index += 1
            else:
                reuse_index = 1
            for row in range(len(self.df_synthetic_reuse_links)):
                reuse_index += 1
                data.append((reuse_index,
                             case_trans[self.df_synthetic_reuse_links['case_id'].iloc[row]],
                             self.df_synthetic_reuse_links['text'].iloc[row],
                             self.df_synthetic_reuse_links['source'].iloc[row],
                             self.df_synthetic_reuse_links['source_text'].iloc[row],
                             self.df_synthetic_reuse_links['reuse_from_source'].iloc[row],
                             self.df_synthetic_reuse_links['ofset'].iloc[row],
                             str(self.df_synthetic_reuse_links['source_matrix'].iloc[row]),
                             str(self.df_synthetic_reuse_links['part_suspect_matrix'].iloc[row]),
                             self.df_synthetic_reuse_links['reuse_size'].iloc[row],))
            table = 'textreuse_syntheticcaselinks'
            fields = "(reuse_index,case_id,text,source,source_text,reuse_from_source,start_offset,source_matrix,part_suspect_matrix,reuse_size)"
            _insert_postgre_rows(table=table, fields=fields,
                                 values=data[:], connection=con)
        else:
            self.df_synthetic_text.to_pickle(name + ".pickle")
            self.df_synthetic_reuse_links.to_pickle(name + "_links.pickle")
            self.df_synthetic_text.to_excel(name + ".xlsx")
            self.df_synthetic_reuse_links.to_excel(name + "_links.xlsx")

    def create_synthetic_data(self, batch_name, cases, text_reuse_prob=0.8, long_tr_probability=0.25, con=None):
        self._clear_local_data()
        for case_id in range(cases):
            new_links, new_tr = self.create_synthetic_text(text_reuse_prob=text_reuse_prob,long_tr_probability=long_tr_probability)
            new_tr['case_id'] = case_id
            self.df_synthetic_text = pd.concat([self.df_synthetic_text,new_tr], sort=False)
            if len(new_links) > 0:
                new_links['case_id'] = case_id
                self.df_synthetic_reuse_links = pd.concat([self.df_synthetic_reuse_links,new_links], sort=False)
        self.store_data(batch_name, con)


class text_reuse_api():

    def __init__(self, host, port, user=None, password=None, secured = False, debug =False):
        if secured:
            self.host = "https://" + host
        else:
            self.host = "http://" + host
        if port != '':
            self.host += ":" + port
        self.SDST = None
        self.token = None
        self.debug = debug
        if user and password:
            self.login(user, password)


    def _create_log(self):
            log_path = "log/"
            timestr = time.strftime("%Y%m%d-%H%M")
            if not os.path.exists(log_path):
                os.makedirs(log_path)
            self.log_filename = log_path + "TR_Cross_validation_" + timestr + '.log'
            self.SDST = logging.getLogger('Text-Reuse-CrossVal')
            self.SDST.setLevel(logging.INFO)
            # Add the log message handler to the logger
            handler = logging.handlers.RotatingFileHandler(self.log_filename,
                                                           maxBytes=1024 * 300,
                                                           backupCount=5)
            self.SDST.addHandler(handler)

    def _log(self, message, timing=False):

        if not self.SDST:
            self.create_log()
        if timing:
            self.SDST.info(message + " ; " + str(datetime.datetime.now()))
        else:
            self.SDST.info(message)
        self.SDST.info("\n")

    def login(self, user, password):
        dt = {"username": user, "password": password}
        payload = json.dumps(dt)
        headers = {'Content-Type': "application/json"}
        url = self.host + "/auth/"
        res = requests.request("POST", url, data=payload, headers=headers)
        result = res.json()
        if 'token' in result:
            self.token = result['token']
        else:
            print("Error: {}".format(result))
        return result

    def get_es_sentence(self,source):
        if not self.token:
            raise Exception("No Token, It seens as you didnt login. try to call the login method first")
        payload = {}
        headers = {'Content-Type': "application/json",
                  'Authorization': 'Token ' + self.token}
        url = self.host + "/textreuse/api/es_source?source_name=" + str(source)
        res = requests.request("GET", url, data=payload, headers=headers)
        return res.json()

    def get_result(self, pk, full=False):
        if not self.token:
            raise Exception("No Token, It seens as you didnt login. try to call the login method first")
        payload = {}
        headers = {'Content-Type': "application/json",
                  'Authorization': 'Token ' + self.token}
        url = self.host + "/textreuse/api/ff_list/" + str(pk)
        if full:
            url += "?full"
        res = requests.request("GET", url, data=payload, headers=headers)
        return res.json()

    def get_source_links(self, pk, source_name):
        if not self.token:
            raise Exception("No Token, It seens as you didnt login. try to call the login method first")
        payload = {}
        headers = {'Content-Type': "application/json",
                  'Authorization': 'Token ' + self.token}
        url = self.host + "/textreuse/api/ff_links/" + str(pk) + "?page=1&page_size=50&source_name="+source_name
        res = requests.request("GET", url, data=payload, headers=headers)
        return res.json()

    def get_links(self, pk, page=1, page_size=100, textreuse=False, direct_links=True):
        if not self.token:
            raise Exception("No Token, It seens as you didnt login. try to call the login method first")
        payload = {}
        headers = {'Content-Type': "application/json",
                  'Authorization': 'Token ' + self.token}
        url = self.host + "/textreuse/api/ff_links/" + str(pk) + "?page=" + str(page) + "&page_size=" + str(page_size)
        if direct_links:
            url += "&link_type=direct"
        if textreuse:
            url += "&textreuse=true"
        res = requests.request("GET", url, data=payload, headers=headers)
        return res.json()

    def get_all_links(self, sub_id, textreuse=False, direct_links=True, max_results=0):

        def add_page(page_results, text_reuse_links):
            for r in page_results:
                text_reuse_links[r['id']] = r

        text_reuse_links = {}
        next_page = 1
        tr_links = self.get_links(sub_id, page=next_page, textreuse=textreuse, direct_links=direct_links)
        while next_page:

            if 'results' in tr_links:
                add_page(tr_links['results'], text_reuse_links)
                if self.debug:
                    pass
                    # print(f"Page: {next_page}, retrieved: {len(text_reuse_links)}")
                if max_results> 0 and len(text_reuse_links) >= max_results:
                    break
            if 'next' not in tr_links or tr_links['next'] == "":
                next_page = 0
            else:
                next_page += 1
                tr_links = self.get_links(sub_id, page=next_page,
                                          page_size=100, textreuse=textreuse, direct_links=direct_links)
        return text_reuse_links

    def submit(self, data):
        if not self.token:
            raise Exception("No Token, It seens as you didnt login. try to call the login method first")
        payload = json.dumps(data)
        headers = {'Content-Type': "application/json",
                  'Authorization': 'Token ' + self.token}
        url = self.host + "/he/textreuse/api/ff_list/"
        try:
            res = requests.request("POST", url, data=payload, headers=headers)
            if res:
                return res.json()
            else:
                print("ERROR: {}, {}".format(res, res.text))
                return None
        except Exception as inst:
            print(type(inst))
            print(inst)
            print(inst.args)
            pass

    def _submit_cross_validation(self, corpora, windows, slides, exact_orders, fuzziness, slopes, query_types, tr_rules, passages, job_name, limit, DEBUG):
        counter = 1
        for passage in passages:
            for win in windows:
                for slide in slides:
                    for fuzz in fuzziness:
                        for slope in slopes:
                            for query_type, operator in query_types:
                                for exact_order in exact_orders:
                                    for tr_rule in tr_rules:
                                        for corpus in corpora:
                                            if type(corpus) == str:
                                                corpus = [corpus]
                                            # text_size = len(ftokens(passage))
                                            dt = {
                                                "passage": passage,
                                                "corpus": corpus,
                                                "win": win,
                                                "slide": slide,
                                                "exact_order": exact_order,
                                                "fuzziness": fuzz,
                                                "slope": win + slope,
                                                "query_type": query_type,
                                                "operator": operator,
                                                "tr_rules": tr_rule,
                                                "output": "None",
                                                "jobs_group_name": job_name}
                                            if DEBUG:
                                                print(dt)
                                                process_id = -1
                                            else:
                                                submit_ret = self.submit(dt)
                                                if submit_ret:
                                                    process_id = submit_ret['id']
                                                else:
                                                    return -1
                                            counter += 1
                                            if limit > 0 and counter > limit:
                                                return 0

    def cross_validation(self, corpora, windows, slides, exact_orders, fuzziness, slopes, query_types, tr_rules, passages, job_name, limit=1, DEBUG=False):
        self._submit_cross_validation(corpora, windows, slides, exact_orders,
                                      fuzziness, slopes, query_types, tr_rules,
                                      passages, job_name, limit, DEBUG)
