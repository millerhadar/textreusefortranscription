import pandas as pd
import numpy as np
import text_reuse
import os
import pickle
import time
import datetime
import logging
import argparse
import warnings
from elasticsearch import Elasticsearch
import difflib as dl


class MyLog():
    def __init__(self,log_path, log_name):
        #log_path = "log/"
        timestr = time.strftime("%Y%m%d-%H%M")
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        log_filename = log_path + log_name+ "_" +timestr + '.log'
        self.SDST = logging.getLogger('Text Reuse')
        self.SDST.setLevel(logging.INFO)
        # Add the log message handler to the logger
        handler = logging.handlers.RotatingFileHandler(log_filename,
                                                       maxBytes=1024 * 300,
                                                       backupCount=5)
        self.SDST.addHandler(handler)

    def log(self, message, timing=True):
        if timing:
            self.SDST.info(message + " ; " + str(datetime.datetime.now()))
        else:
            self.SDST.info(message)
        self.SDST.info("\n")

# A function to submit a new text reuse job
def submit_biblical_sources_job(tr_api, passage, tr_rules, win=3,
                                corpus=["tanach", "mishnah", "bavli", "yerushalmi", "aggadic"],
                                slide=1):
    dt = {
        "passage": passage,
        "corpus": corpus,
        "win": win,
        "slide": slide,
        "exact_order": False,
        "fuzziness": 1,
        "slope": 3,
        "query_type": "query_string",
        "operator": "And",
        "tr_rules": tr_rules,
        "output": "None",
        "indirect_links": False,
        "confusion_matrix": False,
        "store_new_tr_to_db": True
    }
    job = tr_api.submit(dt)
    return job

def load_ranks(path, name='jobs_rank.pickle'):
    if os.path.exists(path + name):
        return pd.read_pickle(path + name)
    else:
        return pd.DataFrame(columns=['gap_penalties', 'maximal_source_tokens', 'source_tokens', 'norm_rank',
                                     'job', 'boostered_rank','row'])

def save_ranks(df, path, name='jobs_rank'):
    df.to_pickle(path + name + ".pickle")
    df.to_excel(path + name + ".xlsx", index=False)

def store_transcription(df, path, name):
    df.to_pickle(path + name + ".pickle")
    df.to_excel(path + name + ".xlsx", index=False)

def maximal_source_string(longest_seq, suspect_tokens, source_tokens, allowed_gaps, seq_pos, token_seq):
    '''
        trim the source text to the maximal extent of tokens that could match the suspect
        based on. the longext matched sequence and the allowed gap.
    '''

    #     start = max(0,min(longest_seq) - int(len(suspect_tokens)*1/1))
    #     stop = max(longest_seq) + int(len(suspect_tokens)*1/1)

    start = max(0, min(longest_seq) - token_seq[min(seq_pos)])
    stop = max(longest_seq) + (len(suspect_tokens) - token_seq[max(seq_pos)]) + 1

    maximal_source_tokens = source_tokens[start:stop]

    return maximal_source_tokens, " ".join(maximal_source_tokens)

def sequencial_rank(s1, s2):
    s = dl.SequenceMatcher(None, s1, s2)
    return (round(s.ratio(), 3))

def raw_sequence(suspect_tokens, source_tokens):
    df = dl.Differ()
    result = list(df.compare(suspect_tokens, source_tokens))
    token_loc = 0
    loc = 0
    eq = []
    teq = []
    for t in result:
        if t[0] == " ":
            eq.append(loc)
            teq.append(token_loc)
            loc += 1
            token_loc += 1
        elif t[0] == "+":
            loc += 1
        elif t[0] == "-":
            token_loc += 1

    return eq, teq

def longest_subseq(arr, max_gap=0, debug=False):
    def _locate_longest_seq(lst):
        '''
        :param lst: ordered list of integers
        :return: a vector of the longest sequence located (in a row = with no gaps)
        '''
        try:
            v = [[[x for x in lst if x > -1][0]]]
            v_pos = 0
            longest_seq = 0
            last_val = v[0][0]
            for i in range(lst.index(v[0][0]) + 1, len(lst)):
                if lst[i] == last_val + 1:
                    v[v_pos].append(lst[i])
                    last_val = lst[i]
                elif lst[i] > last_val:
                    if len(v[v_pos]) > len(v[longest_seq]):
                        longest_seq = v_pos
                    v.append([lst[i]])
                    v_pos += 1
                    last_val = lst[i]
                elif lst[i] < 0:
                    last_val = lst[i]

            if len(v[len(v) - 1]) > len(v[longest_seq]):
                longest_seq = len(v) - 1
            return v[longest_seq]
        except Exception:
            print(lst)
            return []

    def _locate_gaped_seq(lst, longest_sequence_vector, gap):
        gap_penalties = []
        try:
            locate_new_position = True

            while locate_new_position:
                locate_new_position = False
                max_seq = max(longest_sequence_vector)
                min_seq = min([x for x in longest_sequence_vector if x > -1])
                for i in range(1, gap + 1):
                    # next_val = lst.find()
                    if max_seq + i in lst:
                        longest_sequence_vector.append(max_seq + i)
                        gap_penalties.append(i)
                        locate_new_position = True
                    if min_seq - i in lst and min_seq - i > -1:
                        longest_sequence_vector = [min_seq - i] + longest_sequence_vector
                        gap_penalties.append(i)
                        locate_new_position = True
            return longest_sequence_vector, gap_penalties
        except Exception:
            print(longest_sequence_vector)
            return 0, gap_penalties

    lst = arr.copy()
    lst.sort()

    if len(lst) == 0:
        # raise ValueError(f"Bad lst {lst}")
        return [], [], [], []
    longest_sequence_vector = _locate_longest_seq(lst)

    gap_penalties = [0]
    if max_gap:
        longest_sequence_vector, gap_penalties = _locate_gaped_seq(lst, longest_sequence_vector, max_gap)
    longest_seq_position = [arr.index(x) for x in longest_sequence_vector]

    longest_seq_positional_penalty = []

    for idx, x in enumerate(longest_seq_position):
        if idx == 0:
            longest_seq_positional_penalty = [0]
        else:
            longest_seq_positional_penalty.append(abs(x - longest_seq_position[idx - 1] - 1))

    if debug:
        print(
            f"Function:longest_subseq\nThe longest sequence: {longest_sequence_vector}\nThe Positions in the original string: {longest_seq_position}\nThe Sequence penalties: {longest_seq_positional_penalty}\nGap Penalties: {gap_penalties}")
    return longest_sequence_vector, longest_seq_position, longest_seq_positional_penalty, gap_penalties

def get_words_prob(vect, corpus, ptype='idf', zero_if_absent=False, lexicons= None):
    def get_word_prob(vect, corpus, ptype='idf', zero_if_absent=False, lexicons= None ):
        '''
        :param vect: a ord/token to search for its
        :param corpus: the corpus the word is from
        :return: probability or idf of the word in the corpus
        '''
        corpus_shortcut = {"Torah": 'Tanakh-Torah',
                           "Prophets": "Tanakh-Prophets",
                           "Writings": "Tanakh-Writings",
                           "Bavli": "Talmud-Bavli",
                           "Yerushalmi": "Talmud-Yerushalmi",
                           "Aggadic": "Midrash-Aggadic",
                           "Midrash-Aggadic": "Midrash-Aggadic"
                           }
        if not lexicons:
            lexicons = pd.read_pickle("lexicons/all_lexicons_data.pickle")
        try:
            # corpus_dist = self.lexicons[corpus][self.lexicons[corpus]['index']==vect]
            name = corpus_shortcut[corpus]
            corpus_dist = lexicons[name]['lexicon']['token_lexicon'][
                lexicons[name]['lexicon']['token_lexicon']['token'] == vect]
            if len(corpus_dist) > 0:
                if ptype == "idf":
                    return (corpus_dist['idf'].iloc[0])
                else:
                    return (corpus_dist['P'].iloc[0])

            else:
                if zero_if_absent:
                    return 0.0
                if ptype == "idf":
                    return lexicons[name]['lexicon']['quantile'][0.01]
                else:
                    return lexicons[name]['lexicon']['Pquantile'][0.01]
        except Exception:
            return 0.0002
            # Add here call to error log
        return 0.0003

    probs = []
    if not lexicons:
        lexicons = pd.read_pickle("lexicons/all_lexicons_data.pickle")
    for w in vect:
        probs.append(get_word_prob(w, corpus, ptype=ptype, zero_if_absent=zero_if_absent,lexicons=lexicons))
    if len(probs) == 0:
        return 0.0001
    return max(probs)

def query_builder2(txt, fields, operator, corpora, exact_match, fuzziness, size=20):

    def clean_q_txt(txt):
        sym = [("'", ""),('"', ""), ("["," "),("]"," "),("}"," "),("{"," ")]
        for k,v in sym:
            txt = txt.replace(k,v)
        return txt

    #text = txt.replace("'", "").replace('"', "").replace("["," ").replace("]"," ")
    text = clean_q_txt(txt)
    query = {"size": size}
    if fuzziness != 0:
        fuzz = "~" + str(fuzziness) + " "
        q = fuzz.join(text_reuse.ftokens(text)) + fuzz
    if exact_match:
        q = '"' + q + '"'
    qshould = {"query_string": {"fields": fields,
                                "default_operator": operator,
                                "query": q}}
    #

    if len(corpora) > 0:
        cmust_list = [{"match": {"categories": corpus}} for corpus in corpora]
        qmust = {"bool": {"should": cmust_list}}
        #query["query"]["must"] = {"bool": {"should": cmust_list}}
        query["query"] = {"bool": {"should": qshould, "must": qmust}}
    else:
        query["query"] = {"bool": {"should": qshould}}
    return query

def search_line(les, q, index, minimum_results=10):
    result = les.search(index=index, body=q)
    ret = pd.DataFrame()
    if result.get('hits') is not None and result['hits'].get('hits') != []:
        # print(f"Total results: {len(result['hits']['hits'])}, Best Score: {result['hits']['max_score']}, Max Score to fetch: {result['hits']['max_score']*0.9}")

        for hit in result['hits']['hits']:
            ret = ret.append({"target_name": hit['_source']['location'],
                              "score": hit['_score'],
                              "id": hit['_id'],
                              "text": hit['_source']['sentence']}, ignore_index=True)
        # print(f"Source: {hit['_source']['location']}, Score: {hit['_score']}, ID: {hit['_id']}")
        ret['norm_score'] = ret['score'] / float(max(ret['score']))
    return ret

def rank_source(source_tokens, source_text, suspect_tokens, source_name, allowed_gaps=2):
    # Align tokens and get longest sequence
    tseq, token_seq = raw_sequence(suspect_tokens, source_tokens)
    if len(tseq) == 0:
        match_rank = 0
        longest_seq = 0
        seq_positions = []
        seq_penalty = []
        gap_penalties = []
        maximal_source_tokens = []
    else:
        longest_seq, seq_positions, seq_penalty, gap_penalties = longest_subseq(tseq, max_gap=allowed_gaps)

        # Extract maximal sequence from source based on the allowed gaps
        maximal_source_tokens, maximal_source_text = maximal_source_string(longest_seq,
                                                                           suspect_tokens,
                                                                           source_tokens,
                                                                           allowed_gaps,
                                                                           seq_positions,
                                                                           token_seq)

        # claculate distance between suspect and the maximal source sequence
        suspect_text = " ".join(suspect_tokens)
        match_rank = sequencial_rank(suspect_text, maximal_source_text)

    rank_res = {'rank': match_rank,
                "boostered_rank": 0,
                'neighborhood_booster': 0,
                'source_name': source_name,
                'longest_seq': longest_seq,
                'seq_positions': seq_positions,
                'seq_penalty': seq_penalty,
                'gap_penalties': gap_penalties,
                'maximal_source_tokens': maximal_source_tokens,
                'source_tokens': source_tokens,
                'suspect_tokens': suspect_tokens}

    return rank_res

def rank_sources2(links, suspect_tokens, history=None, allowed_gaps=2):
    df_ranks = pd.DataFrame()

    for inx in range(len(links)):
        source_text = text_reuse.clean_text(links['text'].iloc[inx])
        source_tokens = text_reuse.ftokens(source_text)
        target_name = links['target_name'].iloc[inx]

        curr_rank = rank_source(source_tokens, source_text, suspect_tokens, target_name,
                                allowed_gaps=allowed_gaps)

        df_ranks = df_ranks.append(curr_rank, ignore_index=True)
    try:
        df_ranks.sort_values(by=['rank'], inplace=True, ascending=False)
        df_ranks['norm_rank'] = df_ranks['rank'] / max(df_ranks['rank'])
    except Exception:
        pass
    # return  df_ranks[df_ranks['norm_rank']> tollerance]
    return df_ranks

def text_alignment(suspect_tokens, maximal_source_tokens):
    origin = ""
    aligned_text = pd.DataFrame(columns=["token", "origin", "match", "match_id", "location", "last_match_location"])
    for dif in dl.context_diff(suspect_tokens, maximal_source_tokens):
        diff_components = dif.split(" ")
        if len(diff_components) > 2 and diff_components[1] != "":
            loc = 0
            seq_match = 0
            last_match_location = 0
            if origin == "suspect":
                origin = "source"
            else:
                origin = "suspect"
        if origin != "" and diff_components[-1][:3] not in ["***", "---"]:
            if diff_components[0] == "":
                match = 1.0
                seq_match += 1
                match_id = seq_match
                last_match_location = loc
            else:
                match = 0.0
                match_id = 0
            aligned_text = aligned_text.append({"token": diff_components[-1],
                                                "origin": origin,
                                                "match_id": match_id,
                                                "match": match,
                                                "location": loc,
                                                "last_match_location": last_match_location}, ignore_index=True)

            loc += 1
    return aligned_text

def distance_to_ancor(df, token):
    def loc_distance(x, loc):
        return abs(loc - x['location'])

    tmp = df.copy()
    cur_loc = tmp[tmp['token'] == token]['location'].iloc[0]
    # print(cur_loc)
    tmp['loc_dist'] = tmp.apply(loc_distance, args=[cur_loc], axis=1)
    tt = tmp[tmp['match'] == 1.0]
    # print(tt)
    return tt[tt['loc_dist'] == min(tt['loc_dist'])]['loc_dist'].iloc[0]

def extend_align_tokens(df, source_tokens, source_name, lexicons=None, limit=0):

    def pick_close_token(close_to_tested, source_aligned_text):
        for token in close_to_tested:
            tmp = source_aligned_text[source_aligned_text['token'] == token]
            if len(tmp) == 0:
                return -1, ""
            close_to_tested_row = source_aligned_text[source_aligned_text['token'] == token].index[0]
            if df['match'].iloc[close_to_tested_row] == 0.0:
                return close_to_tested_row, df['token'].iloc[close_to_tested_row]
        return -1, ""

    counter = 0
    try:
        next_match_id = max(df['match_id']) + 1
        suspect_aligned_text = df[df['origin'] == 'suspect']
        source_aligned_text = df[df['origin'] == 'source']
        corpus = source_name.split(" ")[0]
    except Exception:
        return df

    for tested_row in range(len(suspect_aligned_text)):
        # tested_row = 0

        if suspect_aligned_text['match'].iloc[tested_row] == 0.0:
            tested = suspect_aligned_text['token'].iloc[tested_row]

            close_to_tested = dl.get_close_matches(tested, source_tokens)
            # print(tested, close_to_tested)
            close_to_tested_row = ""
            if len(close_to_tested) > 0:
                close_to_tested_row, close_to_tested_token = pick_close_token(close_to_tested, source_aligned_text)
                if close_to_tested_row > 0:

                    # close_to_tested_row = source_aligned_text[source_aligned_text['token']== close_to_tested[0]].index[0]

                    closness = dl.SequenceMatcher(None, tested, close_to_tested_token).ratio()

                    tested_in_corpus = get_words_prob([tested], corpus, zero_if_absent=True, lexicons=lexicons)
                    if tested_in_corpus == 0.0:
                        closness *= 1.1

                    # calculate the distance between the selected token to the closest 100% match tokens
                    dist_to_suspect_ancor = distance_to_ancor(suspect_aligned_text, tested)
                    dist_to_source_ancor = distance_to_ancor(source_aligned_text, close_to_tested_token)
                    if dist_to_source_ancor == dist_to_suspect_ancor:
                        closness *= 1.05

                    df.at[tested_row, 'match'] = min(0.99, closness)
                    df.at[tested_row, 'match_id'] = next_match_id
                    df.at[close_to_tested_row, 'match'] = min(0.99, closness)
                    df.at[close_to_tested_row, 'match_id'] = next_match_id
                    next_match_id += 1

            else:
                closness = 0
            counter += 1
            if limit > 0 and counter > limit:
                break

    return df

def cstr(s, color='black'):
    return "<text style=color:{}>{}</text>".format(color, s)

def find_color(item, idx, origin, aligned_text):
    at = aligned_text[(aligned_text['match_id'] > 0) & (aligned_text['origin'] == origin)]
    try:
        match = at[at['token'] == item]['match'].iloc[0]
        if match == 1.0:
            return "green"
        elif match >= 0.9:
            return "springgreen"
        elif match > 0:
            return "orange"
        else:
            return "red"
    except Exception:
        return "red"
    return "red"

def process_job2(suspect_text, row, tollerance, es_params,es,allowed_gaps=2):

    # Get links onlie
    if suspect_text is None or suspect_text == "":
        return None
    q = query_builder2(suspect_text, es_params['fields'],
                       es_params['operator'],
                       es_params['corpora'],
                       es_params['exact_match'],
                       es_params['fuzziness'],
                       size=es_params['query_result_size'])
    #es = Elasticsearch(es_params['ehost'])
    links = search_line(es, q, es_params['index'])

    suspect_text = text_reuse.clean_text(suspect_text)
    suspect_tokens = text_reuse.ftokens(suspect_text)

    df_ranks = rank_sources2(links, suspect_tokens, allowed_gaps=allowed_gaps)
    # df_ranks['job'] = job_id
    df_ranks['row'] = row
    df_ranks['extended'] = 0
    try:
        df_filtered_ranks = df_ranks[df_ranks['norm_rank'] > tollerance]
    except Exception:
        df_filtered_ranks = df_ranks

    return df_filtered_ranks

def restore_rank_phase_2(path, name1='jobs_rank_phase_1.pickle',
                         name2='jobs_rank_phase_2.pickle'):
    def new_job(x):
        if x['row'] in already_processed:
            return 0
        return 1

    df_jobs1 = load_ranks(path=path, name=name1)
    df_jobs2 = load_ranks(path=path, name=name2)
    already_processed = list(set(df_jobs2['row'].to_list()))
    df_jobs1['copy'] = df_jobs1.apply(new_job, axis=1)
    tmp = df_jobs1[df_jobs1['copy'] == 1].copy()
    df_jobs2 = pd.concat([df_jobs2, tmp])

    return df_jobs2

def get_es_sentence_from_source(location,les,index,mlog = None):
    sentence = sentence_tokens = None
    try:
        location = location.replace('"',"'")
        q = '''{"query": {"match": {"location": "'''+ location+ '''" }}}'''
        result = les.search(index=index, body=q)
        if len(result['hits']['hits']) > 0:
            sentence = result['hits']['hits'][0]['_source']['sentence']
        sentence = text_reuse.clean_text(sentence)
        sentence_tokens= text_reuse.ftokens(sentence)
    except Exception as e:
        if mlog is not None:
            mlog.log(f"Error: Get ES Location From Source, Params: location: {location}, index: {index}\nquery: {q}\n{str(e)}")
        pass
    return sentence, sentence_tokens

def extend_job_source(les, index,suspect_tokens, source, mlog=None):
    '''
       Compute the rank for a selected source ( source name ) against the suspect
    '''
    source_text, source_tokens = get_es_sentence_from_source(source,les,index, mlog)
    if source_text is None:
        if mlog is not None:
            mlog.log(f"Error: extend_job_source, Params: suspect_tokens: {str(suspect_tokens)}, index: {index}\n")
        return None
    new_rank = rank_source(source_tokens, source_text, suspect_tokens, source, allowed_gaps=2)
    return new_rank

def extend_neighborhood_sources(df_jobs_rank, suspect_text, manuscript_row, les,index,rows_away=3, mlog=None):
    '''
        for a selected row in the manuscript, find the "popular" sources in its serounding (rows_away)
        and add these sources as potential to the current row.
    '''
    # Find a list of the neighborhood and count the number of appearence of each
    tmp = df_jobs_rank[
        (df_jobs_rank['row'] <= manuscript_row + rows_away) & (df_jobs_rank['row'] >= manuscript_row - rows_away) & (
                df_jobs_rank['extended'] == 0)]['source_name']
    tmp = tmp.value_counts()
    existing_sources = df_jobs_rank[
        (df_jobs_rank['row'] == manuscript_row ) & (df_jobs_rank['extended'] == 0)]['source_name']
    suspect_tokens = text_reuse.ftokens(suspect_text)

    # Take the most probable x sources in the neighborhood and add them to the current manuscript row to be tested
    number_of_sources = 3
    for src in tmp[:number_of_sources].index.to_list():
        if src not in existing_sources:
            new_rank = extend_job_source(les,index,suspect_tokens=suspect_tokens, source=src, mlog=mlog)
            if new_rank is None:
                if mlog is not None:
                    mlog.log(
                        f"Error: extend_neighborhood_sources, Params: manuscript_row: {str(manuscript_row)}, rows_away: {rows_away}\n")
                raise ValueError('extend_job_source failed')
            else:
                new_rank['row'] = manuscript_row
                new_rank['extended'] = 1
                df_jobs_rank = df_jobs_rank.append(new_rank, ignore_index=True)

        # print(f"Source {src}, New Rank: {new_rank['rank']}")
    return df_jobs_rank

def clear_unused_neighborhood_extention(df_jobs_rank, man_row):
    tmp = df_jobs_rank[df_jobs_rank['row'] == man_row].sort_values(by=['boostered_rank'],
                                                                   inplace=False, ascending=False)
    best_score_line = tmp.index[0]
    rows_to_delete = df_jobs_rank[(df_jobs_rank['extended'] == 1) & (df_jobs_rank['row'] == man_row)].index.to_list()
    if best_score_line in rows_to_delete:
        rows_to_delete.pop(rows_to_delete.index(best_score_line))

    if len(rows_to_delete) > 0:
        df_jobs_rank = df_jobs_rank.drop(rows_to_delete)
    #         print(f" Job ID: {job_id}, Rows to delete: {rows_to_delete}")
    #         print(best_score_line)
    return df_jobs_rank

def neighborhood_boost(manuscript_row, df_jobs_rank, distance_bosting, limit=0):
    # Get all records that has been ranked for the job
    # jobs_to_neighborhood_boost = df_jobs_rank[(df_jobs_rank['neighborhood_booster']==0)&(df_jobs_rank['rank']>0)]


    counter = 1

    jobs_to_neighborhood_boost = df_jobs_rank[(df_jobs_rank['row'] == manuscript_row)]

    neighborhood_sources = {}
    for rows_away in range(1, len(distance_bosting) + 1):
        # neighborhood_sources[rows_away] = df_jobs_rank[(df_jobs_rank['job']==job+rows_away)|(df_jobs_rank['job']==job-rows_away)]['source_name'].to_list()
        neighborhood_sources[rows_away] = df_jobs_rank[
            (df_jobs_rank['row'] == manuscript_row + rows_away) | (df_jobs_rank['row'] == manuscript_row - rows_away)][
            'source_name'].to_list()

    for row in range(len(jobs_to_neighborhood_boost)):

        job_index = jobs_to_neighborhood_boost.index[row]
        cur_rank = jobs_to_neighborhood_boost['rank'].iloc[row]
        tested_source = jobs_to_neighborhood_boost['source_name'].iloc[row]
        # tested_source_split = jobs_to_neighborhood_boost['source_name'].iloc[row].split("--")
        # Check the neighborhood for each row
        booster = 0
        number_of_exact_source = 0
        for rows_away in range(1, len(distance_bosting) + 1):

            # neighborhood_sources = df_jobs_rank[(df_jobs_rank['job']==job+rows_away)|(df_jobs_rank['job']==job-rows_away)]['source_name'].to_list()
            dist = [sequencial_rank(tested_source, x) for x in neighborhood_sources[rows_away]]

            if len(dist) > 0:
                if max(dist) == 1.0:
                    booster += distance_bosting[rows_away]
                    number_of_exact_source += 1
                elif max(dist) > 0.98:
                    booster += distance_bosting[rows_away] / 2
                    number_of_exact_source += 0.5

        df_jobs_rank.at[job_index, 'neighborhood_booster'] = booster * number_of_exact_source
        df_jobs_rank.at[job_index, 'boostered_rank'] = cur_rank * (1 + booster * number_of_exact_source)
        counter += 1
        if limit > 0 and counter > limit:
            break
    return df_jobs_rank

def load_aligned_text(ms_name, ms_row,path):
    name = path + "aligned_text_" +ms_name+ "_"+ str(ms_row) + '.pickle'
    if os.path.exists(name):
        with open(name, 'rb') as handle:
            at = pickle.load(handle)
        return at
    else:
        return None

def store_aligned_text(ob, ms_name, ms_row, path):
    name = path + "aligned_text_" + ms_name + "_" + str(ms_row) + '.pickle'
    if not os.path.exists(path):
        os.makedirs(path)
    with open(name, 'wb') as handle:
        pickle.dump(ob, handle, protocol=pickle.HIGHEST_PROTOCOL)


def align_ms_row(ms_name, ms_row, path, df_jobs_rank, lexicons):
    at = load_aligned_text(ms_name, ms_row, path)
    if at:
        source_tokens = at["source_tokens"]
        suspect_tokens = at["suspect_tokens"]
        maximal_source_tokens = at["maximal_source_tokens"]
        source_name = at["source"]
        aligned_text = at["alignment"]
        suspect_html = at["suspect_html"]
        source_html = at["source_html"]
    else:

        df_ranks = df_jobs_rank[df_jobs_rank['row'] == ms_row].copy()
        df_ranks.sort_values(by=['boostered_rank'], inplace=True, ascending=False)
        source_tokens = df_ranks['source_tokens'].iloc[0]
        suspect_tokens = df_ranks['suspect_tokens'].iloc[0]
        maximal_source_tokens = df_ranks['maximal_source_tokens'].iloc[0]
        source_name = df_ranks['source_name'].iloc[0]
        aligned_text = text_alignment(suspect_tokens,
                                      maximal_source_tokens)
        aligned_text = extend_align_tokens(aligned_text,
                                           maximal_source_tokens,
                                           source_name, lexicons=lexicons)
        suspect_color = {idx: find_color(item, idx, 'suspect', aligned_text) for idx, item in enumerate(suspect_tokens)}
        suspect_html = [cstr(word, color=suspect_color[idx]) for idx, word in enumerate(suspect_tokens)]
        source_color = {idx: find_color(item, idx, 'source', aligned_text) for idx, item in
                        enumerate(maximal_source_tokens)}
        source_html = [cstr(word, color=source_color[idx]) for idx, word in enumerate(maximal_source_tokens)]

        at = {"ms_name": ms_name, "ms_row": ms_row, "source": source_name, "Similarity": df_ranks['rank'].iloc[0],
              "alignment": aligned_text, "suspect_tokens": suspect_tokens,
              "maximal_source_tokens": maximal_source_tokens,
              "source_tokens": source_tokens, "suspect_html": suspect_html, "source_html": source_html}
        store_aligned_text(at, ms_name, ms_row, path + "alignment/")
    return aligned_text, suspect_html, source_html

def pass_1_intra_line(df_transcription, path, trans_fname, es_params, allowed_gaps=2, rank_tolerance=0.9,
                      jobs_rank_name='jobs_rank_phase_1.pickle',
                      save_break=100, limit=0, mlog=None):
    if mlog:
        mlog.log("******* Pass I : Intra Line Sources Search Started *****")
    jobs_to_process = df_transcription[(df_transcription['rank'] == 0)]
    df_jobs_rank = load_ranks(path, jobs_rank_name)

    df_raw__jobs_rank = pd.DataFrame()
    counter = 1
    total_jobs = len(jobs_to_process)
    start_time = time.time()
    es = Elasticsearch(es_params['ehost'])
    for row in range(total_jobs):
        job_index = jobs_to_process.index[row]
        manuscript_row = jobs_to_process['row'].iloc[row]
        suspect_text = jobs_to_process['content'].iloc[row]

        df_ranks = process_job2(suspect_text, manuscript_row, rank_tolerance, es_params, es,allowed_gaps)
        if df_ranks is not None:
            df_jobs_rank = pd.concat([df_jobs_rank, df_ranks])
        df_transcription.at[job_index, 'rank'] = 1

        if counter % save_break == 0:
            save_ranks(df_jobs_rank, path=path, name='jobs_rank_phase_1')
            store_transcription(df_transcription, path, trans_fname)
            if mlog:
                row_time = ((time.time() - start_time)/counter)
                eat = round(row_time*(total_jobs-counter),2)/60
                mlog.log(f"SAVED: MS-Row {manuscript_row} proccessed ({counter/total_jobs}), EAT: {eat} Min.")
        if mlog:
            mlog.log(f"MS-Row {manuscript_row} proccessed ({counter}/{total_jobs}")
        counter += 1
        if limit > 0 and counter > limit:
            break
    save_ranks(df_jobs_rank, path=path, name='jobs_rank_phase_1')
    store_transcription(df_transcription, path, trans_fname)
    if mlog:
        mlog.log(f"Completed")
    return df_jobs_rank

def pass_2_inter_lines(df_transcription, distance_bosting, es_params, path, trans_fname,
                       save_break=100, limit=0, mlog=None):
    if mlog:
        mlog.log("******* Pass II : Inter Line Boosting Stated *****")
    # Load fresh jobs ranking calculation
    # df_jobs_rank = load_ranks(path= path,name = 'jobs_rank_phase_2.pickle')
    df_jobs_rank = restore_rank_phase_2(path)
    es = Elasticsearch(es_params['ehost'])
    jobs_to_process = df_transcription[(df_transcription['neighborhood_boost'] == 0) & (df_transcription['rank'] == 1)]
    counter = 1
    start_time = time.time()
    total_jobs = len(jobs_to_process)
    for row in range(total_jobs):
        # job_id = jobs_to_process['job'].iloc[row]
        manuscript_row = jobs_to_process['row'].iloc[row]
        job_index = jobs_to_process.index[row]
        suspect_text = jobs_to_process['content'].iloc[row]

        # Extend row's sources from the "popular" sources in its serounding rows
        df_jobs_rank = extend_neighborhood_sources(df_jobs_rank, suspect_text=suspect_text,
                                                   manuscript_row=manuscript_row,
                                                   les=es, index=es_params['index'],rows_away=3, mlog=mlog)

        # Boost the row's raknding acording to it's serounding sources
        df_jobs_rank = neighborhood_boost(manuscript_row=manuscript_row,
                                          df_jobs_rank=df_jobs_rank,
                                          distance_bosting=distance_bosting, limit=0)

        # df_jobs_rank = clear_unused_neighborhood_extention(df_jobs_rank,job_id)

        df_transcription.at[job_index, 'neighborhood_boost'] = 1

        if counter % save_break == 0:
            save_ranks(df_jobs_rank, path, name='jobs_rank_phase_2')
            store_transcription(df_transcription, path, trans_fname)
            row_time = ((time.time() - start_time) / counter)
            eat = round(row_time * (total_jobs - counter), 2) / 60
            mlog.log(f"SAVED: MS-Row {manuscript_row} proccessed ({counter/total_jobs}), EAT: {eat} Min.")

        # if mlog:
        #     mlog.log(f"MS-Row {manuscript_row} proccessed ({counter})")

        counter += 1
        if limit > 0 and counter > limit:
            break

    save_ranks(df_jobs_rank, path, name='jobs_rank_phase_2')
    store_transcription(df_transcription, path, trans_fname)
    if mlog:
        mlog.log(f"Pass 2 Completed")
        mlog.log(f"--- Update Results to Excel ----")

    result_update(path, df_transcription, trans_fname)

    if mlog:
        mlog.log(f"JOB Completed")

def result_update(path,df_transcription, trans_fname):
    df_jobs_rank = load_ranks(path= path,name = 'jobs_rank_phase_2.pickle')
    completed_jobs = df_transcription[(df_transcription['neighborhood_boost'] == 1)& (df_transcription['sources'] == "")]['row'].to_list()
    for job in  completed_jobs:
        tmp = df_transcription[df_transcription['row']== job]
        job_index = tmp.index[0]

        job_tmp = df_jobs_rank[df_jobs_rank['row']==job]
        job_tmp = job_tmp.sort_values(by=['boostered_rank'], inplace=False,ascending = False)
        try:
            df_transcription.at[job_index,'sources'] = job_tmp['source_name'].iloc[0]
        except Exception:
            pass
    store_transcription(df_transcription, path, trans_fname)

def main():

    parser = argparse.ArgumentParser(description="Transcription Auto-correction")
    parser.add_argument('-l', '--limit', type=int, metavar='', required=False,
                        help='number of itertions')
    parser.add_argument('-p', '--path', type=str, metavar='', required=True,
                        help='Path to the transcription (excel) ')
    parser.add_argument('-f', '--file_name', type=str, metavar='', required=True,
                        help='Transcription File Name')
    parser.add_argument('-e', '--env', type=str, metavar='', required=False,
                        help='Environment to execute (local or cloud). local is default')
    parser.add_argument('-s', '--save_break', type=int, metavar='', required=False,
                        help='The intervals to save results (default 1000)')
    parser.add_argument('-i', '--elastic_index', type=str, metavar='', required=False,
                        help='The Elasticsearch index to use (default rabbinic2)')
    # parser.add_argument('-r', '--rank_tolerance', type=float, metavar='', required=False,
    #                     help='Filrer only candidates that exceeds this toll (default 0.7)')
    parser.add_argument('-g', '--allowed_gaps', type=float, metavar='', required=False,
                        help='allowed gaps (default 2)')
    parser.add_argument('-c', '--corpora', type=str, metavar='', required=False,
                        help='The corpora to search in. default: ["Tanakh", "Aggadic Midrash", "Bavli", "Yerushalmi", "Mishnah"]. use all for all corpora ')
    args = parser.parse_args()

    path = args.path
    env = 'local'
    if args.env:
        env = args.env

        elastic_index = "rabbinic2"
    if args.env:
        elastic_index = args.elastic_index

    #trans_file_name = "RUFUS- Parma 3122 automatic line alignment"
    if args.file_name:
        trans_file_name = args.file_name

    limit = 0
    if args.limit:
        limit = args.limit

    save_break = 1000
    if args.save_break:
        save_break = args.save_break


    corpora = ["Tanakh", "Aggadic Midrash", "Bavli", "Yerushalmi", "Mishnah"]
    if args.corpora:
        corpora = args.corpora.split(",")
        if corpora[0] == "all":
            corpora = []

    rank_tolerance = 0.7
    # if args.rank_tolerance:
    #     rank_tolerance = args.rank_tolerance

    allowed_gaps = 2
    if args.allowed_gaps:
        allowed_gaps = args.allowed_gaps

    es_params = {'query_result_size': 30, 'fuzziness': 1, 'exact_match': False, 'fields': ["sentence"],
                 'operator': "OR", 'corpora': corpora}
    if args.env:
        env = args.env
    if env == "local":
        es_params['ehost'] = 'localhost:9200'
        es_params['index'] = elastic_index
    else:
        es_params['ehost'] = '35.188.145.192:9876'
        es_params['index'] = elastic_index

    warnings.filterwarnings('ignore')

    mlog = MyLog(path, "TransCorUsingTR")


    fl = path + trans_file_name
    df_transcription = pd.read_excel(fl + ".xlsx")
    df_transcription.replace(np.nan, "")
    df_transcription.fillna("", inplace=True)

    df_jobs_rank = pass_1_intra_line(df_transcription, path, trans_file_name, es_params,
                                     allowed_gaps=allowed_gaps,
                                     rank_tolerance=rank_tolerance,
                                     jobs_rank_name='jobs_rank_phase_1.pickle',
                                     save_break=save_break, limit=limit,
                                     mlog=mlog)

    distance_bosting = {1: 0.16, 2: 0.08, 3: 0.04, 4: 0.02, 5: 0.01}
    pass_2_inter_lines(df_transcription, distance_bosting, es_params, path, trans_file_name, save_break=save_break, limit=limit, mlog=mlog)



if __name__ == "__main__":
    main()