class elasticE():
    def __init__(self,elastic_obj,debug=False):
        self.Debug = debug
        self.es = elastic_obj

    def DeleteIndex(self,index_name):
        ret = -1
        if self.es.indices.exists(index=index_name):
            ret = self.es.indices.delete(index=index_name, ignore=[400, 404])
        return ret


    def CreateIndex(self,index_name,settings = None):
        if settings:
            ret = self.es.indices.create(index=index_name, ignore=400, body=settings)
        else:
            ret = self.es.indices.create(index=index_name, ignore=400)
        return ret


    def DeleteAllDocuments(self,index, document_type):
        return self.es.delete_by_query(index=index, doc_type=document_type, body={"query": {"match_all": {}}})


    def LoadIndex(self,df, index, doc_type, fields=[], conversions=[], limit=0):
        if fields == []:
            fields = list(df.columns.values)
        counter = 1
        for i in range(len(df)):
            new_doc = {}
            for field in fields:
                if 'FloatToInt' in conversions and type(df[field].iloc[i]) == float:
                    new_doc[field] = str(int(df[field].iloc[i]))
                else:
                    new_doc[field] = str(df[field].iloc[i])
            self.es.create(index=index, id=i, doc_type=doc_type, body=new_doc)
            counter += 1
            if limit > 0 and counter > limit:
                break
        return self.es.count(index=index, doc_type=doc_type, body={"query": {"match_all": {}}})

    def AddDocument(self,index, doc_type,doc_id,new_doc):
        self.es.create(index=index, id=doc_id  ,doc_type=doc_type, body=new_doc)